from django.http import HttpRequest
from django.test import SimpleTestCase
from django.urls import reverse

from . import views

from django.contrib.auth import get_user_model
User = get_user_model()
from django.test import RequestFactory, TestCase
from django.test.client import Client


class SimpleTest(TestCase):
    def setUp(self):
        self.user = User.objects.create_superuser (username='testing', email='testing@gmail.com', password='josue123456')

    def test_loginSuccess(self):
        print ('Test Success Login')
        client = Client ()
        self.assertEqual (client.login (username='testing', password='josue123456'), True)


    def test_loginfail(self):
        print('Test fail Login')
        client = Client ()
        self.assertEqual (client.login (username='test', password='null'), False)

    def test_home_page_status_code(self):
        print('Test login status ok.')
        response = self.client.get('/')
        self.assertEquals(response.status_code, 200)
